package com.androidegitim.httprequestlibrary;

import android.app.Activity;

/**
 * Created by ardakaplan on 02/02/16.
 * <p/>
 * www.ardakaplan.com
 * <p/>
 * arda.kaplan09@gmail.com
 */
public abstract class RDARequestListener<W> {

    private Activity activity;

    public RDARequestListener(Activity activity) {

        this.activity = activity;
    }

    public abstract void onSuccess(W object);

    public abstract void onFail(Exception exception);
    
    public Activity getActivity() {
        return activity;
    }
}
