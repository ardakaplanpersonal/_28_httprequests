package com.androidegitim.httprequestlibrary.helpers;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

public final class JsonHelpers {


    public static String listToJson(ArrayList<?> list) {

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

        return gson.toJsonTree(list).getAsJsonArray().toString();
    }

    /**
     * Type collectionType = new TypeToken<List<Player>>() { }.getType();
     * <p/>
     * seklinde type verilerek kullanilir
     *
     * @param json
     * @param collectionType
     * @return
     */
    public static ArrayList<?> jsonToList(String json, Type collectionType) {

        try {

            Gson googleJson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

            JsonReader reader = new JsonReader(new StringReader(json));

            reader.setLenient(true);

            return googleJson.fromJson(reader, collectionType);

        } catch (JsonSyntaxException e) {

            throw e;
        }
    }

    public static String objectToJson(Object object) {
        return new Gson().toJson(object);
    }

    public static <W> W jsonToObject(String json, Class<?> klass) throws JsonSyntaxException {

        try {

            Gson gson = new Gson();
            @SuppressWarnings("unchecked")
            W object = (W) gson.fromJson(json, klass);

            return object;

        } catch (JsonSyntaxException e) {

            throw e;
        }
    }
}
