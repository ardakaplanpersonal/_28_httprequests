package com.androidegitim.httprequestlibrary.helpers;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidegitim.httprequestlibrary.RDAResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public final class RDARequestHelper {

    private static final String RESPONSE = "RESPONSE";


    private static void makeRequest(Context context, String URL, final int requestType, final Map<String, String> headers, final String body, final RDAResponse rdaResponse) {

        Response.Listener responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    rdaResponse.onRequestSuccess(response.getString(RESPONSE));

                } catch (JSONException jsonException) {

                    rdaResponse.onRequestFail(jsonException);
                }
            }
        };


        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                rdaResponse.onRequestFail(error);
            }
        };

        JsonObjectRequest stringRequest = new JsonObjectRequest(requestType, URL, body, responseListener, errorListener) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                if (headers == null) {

                    return super.getHeaders();

                } else {

                    return headers;
                }
            }


            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {

                try {

                    String responseBodyString = new String(response.data, "UTF-8");

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put(RESPONSE, responseBodyString);

                    return Response.success(jsonObject, HttpHeaderParser.parseCacheHeaders(response));

                } catch (Exception exception) {

                    return Response.error(new ParseError(exception));
                }
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(6000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue queue = Volley.newRequestQueue(context);

        queue.add(stringRequest);
    }

    public static void makeGetRequest(Context context, String URL, Map<String, String> headers, RDAResponse rdaResponse) {

        makeRequest(context, URL, Request.Method.GET, headers, null, rdaResponse);
    }

    public static void makePostRequest(Context context, String URL, final Map<String, String> headers, String body, RDAResponse rdaResponse) {

        makeRequest(context, URL, Request.Method.POST, headers, body, rdaResponse);
    }

    public static void makePutRequest(Context context, String URL, final Map<String, String> headers, String body, RDAResponse rdaResponse) {

        makeRequest(context, URL, Request.Method.PUT, headers, body, rdaResponse);
    }

    public static void make_X_WWW_FORM_URL_ENCODED_Request(Context context, int requestMethod, String URL, final Map<String, String> headers, final Map<String, String> parameters, final RDAResponse rdaResponse) {


        StringRequest jsonObjRequest = new StringRequest(requestMethod, URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                rdaResponse.onRequestSuccess(response);

            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                rdaResponse.onRequestFail(error);
            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return parameters;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);

        queue.add(jsonObjRequest);
    }
}
