package com.androidegitim.androidegitimlibrary.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class SharedPreferencesHelpers {

    private SharedPreferencesHelpers() {

    }

    private static SharedPreferences getSharedPreferences(Context context) {

        String applicationName = context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();

        return context.getSharedPreferences(applicationName, MODE_PRIVATE);
    }

    public static String getString(Context context, String key) {

        return getSharedPreferences(context).getString(key, "");
    }

    public static void putString(Context context, String key, String value) {

        SharedPreferences.Editor editor = getSharedPreferences(context).edit().putString(key, value);

        editor.apply();
    }

    public static int getInt(Context context, String key) {

        return getSharedPreferences(context).getInt(key, 0);
    }

    public static void putInt(Context context, String key, int value) {

        SharedPreferences.Editor editor = getSharedPreferences(context).edit().putInt(key, value);

        editor.apply();
    }

    public static boolean getBoolean(Context context, String key) {

        return getSharedPreferences(context).getBoolean(key, false);
    }

    public static void putBoolean(Context context, String key, boolean value) {

        SharedPreferences.Editor editor = getSharedPreferences(context).edit().putBoolean(key, value);

        editor.apply();
    }
}