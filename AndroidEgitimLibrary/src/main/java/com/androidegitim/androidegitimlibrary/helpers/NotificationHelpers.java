package com.androidegitim.androidegitimlibrary.helpers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Arda Kaplan on 11.01.2018 - 02:16
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public final class NotificationHelpers {

    private NotificationHelpers() {

    }


    public static void cancelNotification(Context context, int notificationId) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {

            notificationManager.cancel(notificationId);
        }
    }

    public static void showNotification(Context context,
                                        String title,
                                        String text,
                                        Intent intent,
                                        int drawableID,
                                        boolean showTime,
                                        Integer notificationID,
                                        boolean deletable) {

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context,
                                               context.getApplicationInfo().loadLabel(context.getPackageManager()).toString());

        builder.setContentTitle(title);

        builder.setContentText(text);

        builder.setSmallIcon(drawableID);

        if (intent != null) {

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(context,
                                                                    (int) System.currentTimeMillis(),
                                                                    intent,
                                                                    PendingIntent.FLAG_CANCEL_CURRENT);
            builder.setContentIntent(pendingIntent);
        }

        builder.setShowWhen(showTime);


        Notification notification = builder.build();

        if (deletable) {

            notification.flags |= Notification.FLAG_AUTO_CANCEL;

        } else {

            notification.flags = Notification.FLAG_ONGOING_EVENT;
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationID == null) {

            notificationID = -1990;
        }

        if (notificationManager != null) {

            notificationManager.notify(notificationID, notification);
        }

    }
}
