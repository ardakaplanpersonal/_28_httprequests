
package com.androidegitim.httprequests;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.androidegitim.androidegitimlibrary.helpers.DialogHelpers;
import com.androidegitim.androidegitimlibrary.helpers.RDALogger;
import com.androidegitim.httprequestlibrary.RDAResponse;
import com.androidegitim.httprequestlibrary.helpers.JsonHelpers;
import com.androidegitim.httprequestlibrary.helpers.RDARequestHelper;
import com.androidegitim.httprequests.models.Person;
import com.androidegitim.httprequests.models.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_edittext_name)
    EditText nameEditText;
    @BindView(R.id.main_edittext_surname)
    EditText surnameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        RDALogger.start(getString(R.string.app_name)).enableLogging(true);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.main_button_get)
    public void get() {

        String name = nameEditText.getText().toString();

        String surname = surnameEditText.getText().toString();

        String URL = "https://postman-echo.com/get?" + "name=" + name + "&surname=" + surname;

        RDALogger.info("GET İSTEĞİ YAPILACAK");

        RDALogger.info("İSTEK YAPILAN URL : " + URL);

        final ProgressDialog progressDialog = new ProgressDialog(this);

        progressDialog.setMessage("Lütfen Bekleyiniz.");

        progressDialog.show();

        RDARequestHelper.makeGetRequest(getApplicationContext(), URL, null, new RDAResponse() {

            @Override
            public void onRequestSuccess(String comingJson) {

                progressDialog.dismiss();

                RDALogger.info("GELEN CEVAP " + comingJson);

                Result result = JsonHelpers.jsonToObject(comingJson, Result.class);

                RDALogger.info("GELEN CEVABIN JAVA SINIFINA DÖNÜŞTÜRÜLMÜŞ HALİ\n" + result.toString());

            }

            @Override
            public void onRequestFail(Exception exception) {

                progressDialog.dismiss();

                exception.printStackTrace();

                DialogHelpers.showDialog(MainActivity.this, "Hata", "Bir hata oluştu lütfen tekrar deneyiniz", "Tamam", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                }, null, null, null, null);
            }
        });

    }

    @OnClick(R.id.main_button_post)
    public void post() {

        Person person = new Person();

        person.setName(nameEditText.getText().toString());

        person.setSurname(surnameEditText.getText().toString());

        RDALogger.info("PERSON JAVA SINIFI " + person);

        String personJson = JsonHelpers.objectToJson(person);

        RDALogger.info("PERSON JSON " + personJson);


        final ProgressDialog progressDialog = new ProgressDialog(this);

        progressDialog.setMessage("Lütfen Bekleyiniz.");

        progressDialog.show();

        RDALogger.info("POST İSTEĞİ YAPILACAK");

        String URL = "https://postman-echo.com/post";

        RDALogger.info("İSTEK YAPILAN URL : " + URL);

        RDALogger.info("İSTEK BODY : " + personJson.toString());


        Map<String, String> headers = new HashMap<>();

        headers.put("key1", "1231234123");
        headers.put("sessionID", "asdasd23432");

        RDARequestHelper.makePostRequest(getApplicationContext(), URL, headers, personJson, new RDAResponse() {

            @Override
            public void onRequestSuccess(String comingJson) {

                progressDialog.dismiss();

                RDALogger.info("GELEN CEVAP " + comingJson);

                Result result = JsonHelpers.jsonToObject(comingJson, Result.class);

                RDALogger.info("GELEN CEVABIN JAVA SINIFINA DÖNÜŞTÜRÜLMÜŞ HALİ\n" + result.toString());
            }

            @Override
            public void onRequestFail(Exception exception) {

                progressDialog.dismiss();

                DialogHelpers.showDialog(MainActivity.this, "Hata", "Bir hata oluştu lütfen tekrar deneyiniz", "Tamam", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                }, null, null, null, null);

                exception.printStackTrace();
            }
        });
    }
}
