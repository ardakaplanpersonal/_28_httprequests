package com.androidegitim.httprequests.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arda Kaplan on 24.02.2018 - 21:46
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Headers {

    private String host;
    @SerializedName("accept-encoding")
    private String acceptEncoding;
    @SerializedName("user-agent")
    private String userAgent;
    @SerializedName("x-forwarded-port")
    private String xForwardedPort;
    @SerializedName("x-forwarded-proto")
    private String getxForwardedProto;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getAcceptEncoding() {
        return acceptEncoding;
    }

    public void setAcceptEncoding(String acceptEncoding) {
        this.acceptEncoding = acceptEncoding;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getxForwardedPort() {
        return xForwardedPort;
    }

    public void setxForwardedPort(String xForwardedPort) {
        this.xForwardedPort = xForwardedPort;
    }

    public String getGetxForwardedProto() {
        return getxForwardedProto;
    }

    public void setGetxForwardedProto(String getxForwardedProto) {
        this.getxForwardedProto = getxForwardedProto;
    }

    @Override
    public String toString() {
        return "Headers{" +
                "host='" + host + '\'' +
                ", acceptEncoding='" + acceptEncoding + '\'' +
                ", userAgent='" + userAgent + '\'' +
                ", xForwardedPort='" + xForwardedPort + '\'' +
                ", getxForwardedProto='" + getxForwardedProto + '\'' +
                '}';
    }
}
