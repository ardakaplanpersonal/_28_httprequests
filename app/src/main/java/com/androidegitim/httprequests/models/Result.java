package com.androidegitim.httprequests.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Arda Kaplan on 24.02.2018 - 21:44
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Result {

    @SerializedName("args")
    private Arguments arguments;
    private Headers headers;
    private String url;
    private Json json;



    public Arguments getArguments() {
        return arguments;
    }

    public void setArguments(Arguments arguments) {
        this.arguments = arguments;
    }

    public Headers getHeaders() {
        return headers;
    }

    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Json getJson() {
        return json;
    }

    public void setJson(Json json) {
        this.json = json;
    }

    @Override
    public String toString() {
        return "Result{" +
                "arguments=" + arguments +
                ", headers=" + headers +
                ", url='" + url + '\'' +
                ", json=" + json +
                '}';
    }
}
